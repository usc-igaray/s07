from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('comere', views.comere, name='comere')
]